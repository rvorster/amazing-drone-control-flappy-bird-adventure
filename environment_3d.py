import numpy as np
import matplotlib.pyplot as plt

from planning.rrt import RRT, Node
from planning.smooth import TrajectoryGeneration
from control.controller import GeometricControl
from control.model import Quadrotor
from trajectories.utils import pack_traj

import sim_plotting

model = Quadrotor()

# (x, y, z, xx, yy, zz)
obstacles = [
    [0, 0.5, 0, 1, 1, 0.5],
    [0, 0, 2, 1, 1, 0.5],
    [0, 1.25, 0, 0.5, 0.25, 5],
    [0, 0.2, 0, 0.5, 0.25, 5],
    [0.5, 0.7, 0, 0.5, 0.25, 5],
    [0, 1.8, 0, 1, 0.2, 3.5],
]

# (x, y, z)
bounds = [1, 2, 5]
start = [0.1, 0.1, 0]
goal = [0.1, 2, 4]
margins = [0.05, 0.05, 0.05]

def sim(f_trajectory, f_control, waypoints=[], t_max=5, dt=0.01, render=True):
    # Intialize
    model.t_step = dt
    s = model.reset(position=f_trajectory(0)['x'])

    # Log
    pos, pos_des = [], []
    ts = []

    t = 0
    while (t < t_max):
        traj = f_trajectory(t)
        s = model.step(f_control(traj, s))

        # Log
        pos.append(s['x'])
        pos_des.append(traj['x'])

        # Update
        ts.append(t)
        t += dt

    ts = np.array(ts)
    pos, pos_des = np.array(pos), np.array(pos_des)

    if render:
        sim_plotting.render(pos, pos_des, start, goal, waypoints=waypoints,
                            obstacles=obstacles, bounds=bounds)

if __name__ == '__main__':
    scale = 1

    np.random.seed(10)

    # Path
    planner = RRT(start, goal, bounds, n_nodes=2000, radius=0.5, step=0.5,
                  obstacles=obstacles, dim=3, informed=True, margins=margins)
    planner.plan()
    path = planner.usable_path(scale)

    # Smooth trajectory
    trajectory = TrajectoryGeneration(path, max_iter=100, gamma=5e4, max_speed=1, constrain=True)
    trajectory.obstacles, trajectory.margins = obstacles, margins
    trajectory.solve(scale)

    f_control = GeometricControl().control
    f_trajectory = lambda t: pack_traj(*trajectory.eval(t))

    t_max = min(max(trajectory.t_total, 0), 100)

    sim(f_trajectory, f_control, waypoints=path, t_max=t_max, render=True)