import matplotlib.pyplot as plt
from matplotlib import animation
from mpl_toolkits.mplot3d import axes3d
import numpy as np

# Render animation
def render(pos, pos_des, start, goal, waypoints=[], obstacles=[], bounds=[]):
    # Unpack
    x, y, z = pos[:,0], pos[:,1], pos[:,2]
    xd, yd, zd = pos_des[:,0], pos_des[:,1], pos_des[:,2]

    # Init figure
    fig_anim = plt.figure()
    ax_anim = axes3d.Axes3D(fig_anim, auto_add_to_figure=False)

    fig_anim.add_axes(ax_anim)

    if bounds:
        x_max, y_max, z_max = bounds
        ax_anim.set_xlim([0, x_max])
        ax_anim.set_ylim([0, y_max])
        ax_anim.set_zlim([0, z_max])

    for obstacle in obstacles:
        plot_cube(ax_anim, obstacle)

    ax_anim.set_xlabel('x')
    ax_anim.set_ylabel('y')
    ax_anim.set_zlabel('z')

    ax_anim.set_title('3D path planning')
    ax_anim.grid()

    # Draw start/goal
    ax_anim.scatter(*start, s=25, color='green', label='Start')
    ax_anim.scatter(*goal, s=25, color='blue', label='Goal')

    # Draw waypoints
    x_w, y_w, z_w = waypoints.T
    ax_anim.plot(x_w, y_w, z_w, '-o', color='black', label='Waypoints')

    # Define plots
    line_real, = ax_anim.plot([x[0]], [y[0]], [z[0]], color='red', label='Real Trajectory')
    line_des, = ax_anim.plot([xd[0]], [yd[0]], [zd[0]], color='blue', label='Desired Trajectory')

    point, = ax_anim.plot([x[0]], [y[0]], [z[0]], 'o', color='red', label='Quadrotor')
    point_des, = ax_anim.plot([x[0]], [y[0]], [z[0]], 'o', color='blue', label='Quadrotor')

    def anim(i):
        x_c, y_c, z_c = x[:i + 1], y[:i + 1], z[:i + 1]
        xd_c, yd_c, zd_c = xd[:i + 1], yd[:i + 1], zd[:i + 1]

        # Update real
        line_real.set_xdata(x_c)
        line_real.set_ydata(y_c)
        line_real.set_3d_properties(z_c)

        # Update desired
        line_des.set_xdata(xd_c)
        line_des.set_ydata(yd_c)
        line_des.set_3d_properties(zd_c)

        # Update current
        point.set_xdata(x[i])
        point.set_ydata(y[i])
        point.set_3d_properties(z[i])

        # Update current desired
        point_des.set_xdata(xd[i])
        point_des.set_ydata(yd[i])
        point_des.set_3d_properties(zd[i])

        # Update limits
        if not bounds:
            ax_anim.set_xlim([min(*x_c, *xd_c, -1), max(*x_c, *xd_c, 1)])
            ax_anim.set_ylim([min(*y_c, *yd_c, -1), max(*y_c, *yd_c, 1)])
            ax_anim.set_zlim([min(*z_c, *zd_c, -1), max(*z_c, *zd_c, 1)])

    a = animation.FuncAnimation(fig_anim, anim, frames=len(x), interval=1, repeat=True)

    plt.show()

# Plot cube obstacle (https://stackoverflow.com/questions/33540109/plot-surfaces-on-a-cube)
def plot_cube(ax, obstacle):
    x, y, z, xx, yy, zz = obstacle

    phi = np.arange(1, 10, 2)*np.pi/4
    phi, theta = np.meshgrid(phi, phi)

    x += xx*(0.5 + np.cos(phi)*np.sin(theta))
    y += yy*(0.5 + np.sin(phi)*np.sin(theta))
    z += zz*(0.5 + np.cos(theta)/np.sqrt(2))

    ax.plot_surface(x, y, z, alpha=0.25)
    ax.plot_wireframe(x, y, z, alpha=0.02, color='white')
