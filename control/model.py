import numpy as np
import scipy.integrate
from scipy.spatial.transform import Rotation

from .params import params

class Quadrotor():
    def __init__(self):
        self.dt = 0.01

    def reset(self, position=[0, 0, 0], yaw=0, pitch=0, roll=0):
        '''
        state is a 13 dimensional vector
            postion*3 velocity*3 attitude(quaternion)*4 angular velocity*3
        state = [x y z dx dy dz qw qx qy qz r p q]
        dot_state = [dx dy dz ddx ddy ddz dqw dqx dqy dqz dr dp dq]
        '''

        s = np.zeros(13)
        s[0:3] = position

        q = Rotation.from_euler('zyx', [yaw, pitch, roll]).as_quat()
        s[6:10] = q

        self.state = self._unpack_state(s)
        return self.state

    def step(self, cmd_rotor_speeds, desired_position=[0, 0, 0]):
        rotor_thrusts = params.k_thrust*np.power(cmd_rotor_speeds, 2)

        # Convert speeds to moments/forces
        F = params.control_m@rotor_thrusts
        T, M = F[0], F[1:]

        # Form autonomous ODE for constant inputs and integrate one time step
        def s_dot_fn(t, s):
            return self._s_dot_fn(t, s, T, M)

        # Solve initial value problem
        s = self._pack_state(self.state)
        sol = scipy.integrate.solve_ivp(s_dot_fn, (0, self.dt), s, first_step=self.dt)
        s = sol['y'][:,-1]

        self.state = self._unpack_state(s)
        return self.state

    def _s_dot_fn(self, t, s, u1, u2):
        """
        Compute derivative of state for quadrotor given fixed control inputs as
        an autonomous ODE.
        """

        state = self._unpack_state(s)
        # Position derivative.
        x_dot = state['v']

        # Velocity derivative.
        R = Rotation.from_quat(state['q']).as_matrix()
        v_dot = (R@np.array([0, 0, u1]).T - np.array([0, 0, params.mass*params.g]).T)/params.mass

        # Orientation derivative.
        q_dot = self.quat_dot(state['q'], state['w'])

        # Angular velocity derivative. page 26 Equation 4
        w_dot = params.I_inv@(u2 - np.cross(state['w'], params.I@state['w']))

        # Pack into vector of derivatives.
        return np.hstack([x_dot, v_dot, q_dot, w_dot])

    @staticmethod
    def rotate_k(q):
        """
        Rotate the unit vector k by quaternion q. This is the third column of
        the rotation matrix associated with a rotation by q.
        """
        return np.array([2 * (q[0] * q[2] + q[1] * q[3]),
                         2 * (q[1] * q[2] - q[0] * q[3]),
                         1 - 2 * (q[0] ** 2 + q[1] ** 2)])

    @staticmethod
    def quat_dot(quat, omega):
        """
        Parameters:
            quat, [i,j,k,w]
            omega, angular velocity of body in body axes

        Returns
            quat_dot, [i,j,k,w]

        """
        # Adapted from "Quaternions And Dynamics" by Basile Graf.
        (q0, q1, q2, q3) = (quat[0], quat[1], quat[2], quat[3])
        G = np.array([[ q3,  q2, -q1, -q0],
                    [-q2,  q3,  q0, -q1],
                    [ q1, -q0,  q3, -q2]])
        quat_dot = 0.5 * G.T @ omega

        # Augment to maintain unit quaternion.
        quat_err = np.sum(quat**2) - 1
        quat_err_grad = 2 * quat

        return quat_dot - quat_err * quat_err_grad

    @staticmethod
    def hat_map(s):
        """
        Given vector s in R^3, return associate skew symmetric matrix S in R^3x3
        """
        return np.array([[0, -s[2], s[1]],
                         [s[2], 0, -s[0]],
                         [-s[1], s[0], 0]])

    @staticmethod
    def _pack_state(state):
        """
        Convert a state dict to Quadrotor's private internal vector representation.
        """
        return np.hstack([*state.values()])

    @staticmethod
    def _unpack_state(s):
        """
        Convert Quadrotor's private internal vector representation to a state dict.
        """
        return {'x': s[0:3], 'v': s[3:6], 'q': s[6:10], 'w': s[10:13]}