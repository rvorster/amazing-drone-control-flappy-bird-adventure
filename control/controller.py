import numpy as np
from scipy.spatial.transform import Rotation

from .params import params

# Convert to rotor speeds
def forces_to_command_speeds(u):
    F_i = params.control_m_inv@u
    F_i = np.maximum(F_i, 0)

    # Calc commanded speeds
    cmd_motor_speeds = np.sqrt(F_i/params.k_thrust)
    cmd_motor_speeds = np.minimum(np.maximum(cmd_motor_speeds, params.w_rotor_min), params.w_rotor_max)

    return cmd_motor_speeds

class GeometricControl():
    def __init__(self):
        # Altitude control gains
        self.Kp = np.diag([35, 35, 200])
        self.Kd = np.diag([10, 10, 35])

        # Attitude control gains
        self.Kp_t = np.diag([2000, 2000, 1000])
        self.Kd_t = np.diag([120, 120, 250])

    @staticmethod
    def to_unit(v):
        return v/np.linalg.norm(v)

    @staticmethod
    def vee(R):
        return np.array([-R[1,2], R[0,2], -R[0,1]])

    def control(self, trajectory, state):
        '''
        :param desired state: dictionary with keys {pos, vel, acc, yaw, yaw_dot}
        :param current state: dictionary with keys {pos, vel, euler, omega}
        :return: w_actions
        '''

        ## Altitude control
        ep = state['x'] - trajectory['x']
        ed = state['v'] - trajectory['v']

        # Calculate upward force
        acc = trajectory['acc'] - self.Kp@ep - self.Kd@ed
        F_c = params.mass*acc + np.array([0, 0, params.mass*params.g])

        # Rotate
        R = Rotation.from_quat(state['q']).as_matrix()
        b_3 = R@np.array([0, 0, 1]).T

        # Altitude out
        u_1 = b_3.T@F_c
        b_3c = self.to_unit(F_c)

        ## Attitude controller
        psi_des = trajectory['yaw']
        a_psi = np.array([np.cos(psi_des), np.sin(psi_des), 0])

        b_2c = self.to_unit(np.cross(b_3c, a_psi))

        # Desired orientation
        R_des = np.vstack([np.cross(b_2c, b_3c), b_2c, b_3c]).T
        [phi_des, theta_des, _] = Rotation.from_matrix(R_des).as_euler('xyz')

        e_R = self.vee(R_des.T@R - R.T@R_des)/2

        # Omega error
        p_des, q_des, r_des = 0, 0, trajectory['yaw_dot']
        e_w = state['w'] - np.array([p_des, q_des, r_des])

        u_2 = params.I@(-self.Kp_t@e_R - self.Kd_t@e_w)

        # Output
        u = np.hstack([u_1, u_2])
        return forces_to_command_speeds(u)