import numpy as np

# Bunch of parameters and some common stuff
class Params():
    g = 9.80665

    # Drone parameters
    mass = 0.030 # Mass [kg]
    I = np.array([[1.43e-5, 0, 0],
                [0, 1.43e-5, 0],
                [0, 0, 2.89e-5]])  # Inertial tensor [m2kg]
    I_xx, I_yy, I_zz = I[0,0], I[1, 1], I[2, 2]

    L = 0.046  # Arm length [m]

    k_thrust = 2.3e-8
    k_drag = 7.8e-11

    w_rotor_min = 0 # Min rotor speed [rad/s]
    w_rotor_max = 2500 # Max rotor speed [rad/s]

    @property
    def I_inv(self):
        return np.linalg.inv(self.I)

    @property
    def k(self):
        return self.k_drag/self.k_thrust

    # Control matrix
    @property
    def control_m(self):
        return [[1, 1, 1, 1],
                [0, self.L, 0, -self.L],
                [-self.L, 0, self.L, 0],
                [self.k, -self.k, self.k, -self.k]]

    @property
    def control_m_inv(self):
        return np.linalg.inv(self.control_m)

params = Params()