import numpy as np
import matplotlib.pyplot as plt


#Import results from text file, make sure that the text file is in the same folder
#First column is number of nodes
#Second column is cost, will be zero if no path was found
#Third column is time until a solution was found, will also be zero if no path was found
#Fourth column is informed on or off
results = np.loadtxt("rrt_results_2D_2", skiprows=1)


#print(results.shape)

n_nodes = np.unique(results[:,0])

avg_uninformed = []
avg_informed = []
nodes = []

for i in range(len(n_nodes)):
    
    cost_n_nodes = results[results[:,0]==n_nodes[i]][:,1:4]
    
    cost_informed = cost_n_nodes[cost_n_nodes[:,-1] == 1][:,:2]
    cost_uninformed = cost_n_nodes[cost_n_nodes[:,-1] == 0][:,:2]
    
    cost_time_informed = cost_informed[cost_informed != [0,0]]
    cost_time_informed = np.reshape(cost_time_informed,((int(cost_time_informed.shape[0]/2),2)))
    time_informed = cost_time_informed[:,1]
    
    cost_time_uninformed = cost_uninformed[cost_uninformed != [0,0]]
    cost_time_uninformed = np.reshape(cost_time_uninformed,((int(cost_time_uninformed.shape[0]/2),2)))
    time_uninformed = cost_time_uninformed[:,1]
    
    avg_cost_informed = np.sum(cost_time_informed[:,0])/cost_time_informed.shape[0]
    avg_cost_time_informed = np.sum(cost_time_informed[:,0]*cost_time_informed[:,1])/cost_time_informed.shape[0]
    avg_time_informed = np.sum(time_informed)/time_informed.shape[0]
    
    avg_cost_uninformed = np.sum(cost_time_uninformed[:,0])/cost_time_uninformed.shape[0]
    avg_cost_time_uninformed = np.sum(cost_time_uninformed[:,0]*cost_time_uninformed[:,1])/cost_time_uninformed.shape[0]
    avg_time_uninformed = np.sum(time_uninformed)/time_uninformed.shape[0]
    
    #c_norm = cost_time_informed[:,0]/max(cost_time_informed[:,0])
    #print(c_norm)
    #ct_norm_informed = (np.sum(cost_time_informed[:,0]/max(cost_time_informed[:,0]))*np.sum(cost_time_informed[:,1]/max(cost_time_informed[:,1])))/(cost_time_informed.shape[0])
    #ct_norm_uninformed = np.sum((cost_time_uninformed[:,0]*cost_time_uninformed[:,1])/(max(cost_time_uninformed[:,0])*max(cost_time_uninformed[:,1])))/(cost_time_uninformed.shape[0])
    #print(ct_norm_uninformed)
    
    avg_informed.append([avg_cost_informed,avg_cost_time_informed,avg_time_informed])
    avg_uninformed.append([avg_cost_uninformed,avg_cost_time_uninformed,avg_time_uninformed])
    nodes.append(int(n_nodes[i]))

avg_informed = np.array(avg_informed)
avg_uninformed = np.array(avg_uninformed)
#print(avg_informed)
#print(avg_uninformed)



x = np.arange(len(avg_informed))

#Regular barcharts showing average cost
width = 0.25
plt.bar(x,avg_informed[:,0], width=width, label="informed")
plt.bar(x + 0.25,avg_uninformed[:,0], width=width ,label="uninformed")
plt.ylim(max(avg_informed[:,0])-100,max(avg_informed[:,0])+100)
plt.ylabel("Average cost")
plt.xlabel("Number of nodes")
plt.legend(loc="lower right")
plt.xticks(x + width/2,nodes)
plt.show()

"""
#barcharts showing average cost/time
plt.bar(x,avg_informed[:,3], width=width, label="informed")
plt.bar(x + 0.25,avg_uninformed[:,3], width=width ,label="uninformed")
plt.ylabel("Average normalized cost*time")
plt.xlabel("Number of nodes")
plt.legend(loc="upper right")
plt.xticks(x + width/2,nodes)
plt.show()
"""

#barcharts showing average time
plt.bar(x,avg_informed[:,2], width=width, label="informed")
plt.bar(x + 0.25,avg_uninformed[:,2], width=width ,label="uninformed")
plt.ylabel("Average time")
plt.xlabel("Number of nodes")
plt.legend(loc="upper right")
plt.xticks(x + width/2,nodes)
plt.show()


plt.subplot(2,1,1)
plt.tight_layout(pad=0.0,h_pad=0)
plt.bar(x,avg_informed[:,0], width=width, label="informed")
plt.bar(x + 0.25,avg_uninformed[:,0], width=width ,label="uninformed")
plt.ylim(max(avg_informed[:,0])-100,max(avg_informed[:,0])+100)
plt.ylabel("Average cost")
#plt.xlabel("Number of nodes")
plt.legend(loc="upper right")
#plt.xticks(x + width/2,None)
plt.tick_params(left = True, right = False , labelleft = True ,
                labelbottom = False, bottom = False)

plt.subplot(2,1,2)
plt.tight_layout(pad=-0.1,h_pad=-0.5)
plt.bar(x,avg_informed[:,2], width=width, label="informed")
plt.bar(x + 0.25,avg_uninformed[:,2], width=width ,label="uninformed")
plt.ylabel("Average time")
plt.xlabel("Number of nodes")
#plt.legend(loc="upper right")
plt.xticks(x + width/2,nodes)
plt.show()