import numpy as np

def pack_traj(pos, vel, acc=[0, 0, 0], yaw=0, yaw_dot=0):
    return {'x': np.array(pos), 'v': np.array(vel), 'acc': np.array(acc),\
        'yaw': yaw, 'yaw_dot': yaw_dot}

def traj_from_line(start, end, t_total, t_now):
    vel = (end - start)*2/t_total
    pos = start + t_now*vel/2
    return pos, vel

def traj_from_points(xs, t_step, t, dt):
    i = int(t//t_step)
    t_rel = t - i*t_step

    pos, vel, acc = xs[-1], [0, 0, 0], [0, 0, 0]
    if i + 1 < len(xs):
        pos, _ = traj_from_line(xs[i], xs[i + 1], t_step, t_rel)
        pos_next, _ = traj_from_line(xs[i], xs[i + 1], t_step, t_rel + dt)
        pos_next_next, _ = traj_from_line(xs[i], xs[i + 1], t_step, t_rel + 2*dt)

        vel = (pos_next - pos)/dt
        acc = (pos_next_next - 2*pos_next + pos)/(dt*dt)

    return pos, vel, acc
