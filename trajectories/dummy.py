import numpy as np

from .utils import *

# Testing/tuning -> NOTE: 2D only in x and z (or y and z)
def f_trajectory_dummy(t, dt):
    pos = [0, 0, 1] if t > 0.1 else [0, 0, 0]
    pos = [1, 0, 1] if t > 1 else pos
    pos = [1.5, 0, 1] if t > 2 else pos

    return pack_traj(pos, [0, 0, 0])
