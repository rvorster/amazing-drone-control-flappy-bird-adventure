import pygame as pg
import os
import random
import numpy as np
from scipy.spatial.transform import Rotation

from planning.rrt import RRT, Node
from planning.smooth import TrajectoryGeneration
from control.controller import GeometricControl
from control.model import Quadrotor
from trajectories.utils import pack_traj

# np.random.seed(42)
random.seed(250) # 501 is nice, 252 is nice

WIDTH, HEIGHT = 1500, 740

WIN = pg.display.set_mode((WIDTH, HEIGHT))                                      # Set a window size
pg.display.set_caption("Amazing drone control flappy bird adventure")

RED = (255, 0, 0)
WHITE = (255, 255, 255)
BLUE = (0, 0, 255)
BLACK = (0, 0, 0)
GREEN = (50, 205, 50)

FPS = 60
VEL = 5
N_WALL = 5
ANGLE = 0

DRONE_WIDTH, DRONE_HEIGHT = 90, 18
DRONE_IMAGE = pg.image.load(os.path.join('images', 'drone.png'))                # Import the drone image
DRONE = pg.transform.scale(DRONE_IMAGE, (DRONE_WIDTH, DRONE_HEIGHT))            # Scale the drone image
DRONE_FORWARDS = pg.transform.rotate(DRONE, ANGLE)                              # Rotate the drone for moving forwards
DRONE_BACKWARDS = pg.transform.rotate(DRONE, ANGLE)                             # Rotate the drone for moving backwards

BACKGROUND_IMAGE = pg.image.load(os.path.join('images', 'background.png'))      # Import the background image
BACKGROUND = pg.transform.scale(BACKGROUND_IMAGE, (WIDTH, HEIGHT))              # Sclae the background image

START_POS_x, START_POS_y = 100, 100                                             # Start position
END_POS_x, END_POS_y = 650, 700                                                  # End position
START_POS_d_x = START_POS_x - 0.5 * DRONE_WIDTH                                 # Start position of the drone of the x-axis
START_POS_d_y = START_POS_y - 0.5 * DRONE_HEIGHT                                # Start position of the drone of the y-axis
END_POS_d_x = END_POS_x - 0.5 * DRONE_WIDTH
END_POS_d_y = END_POS_y - 0.5 * DRONE_HEIGHT

HIT_WALL = pg.USEREVENT + 1                                                     # Make an event

def draw_window(drone, drone_angle, wall_top, wall_bottom):                     # Function to visualize objects on the window
    WIN.blit(BACKGROUND, (0, 0))                                                # Set the background

    for rect in [*wall_top, *wall_bottom]:
        pg.draw.rect(WIN, GREEN, rect)

    pg.draw.circle(WIN, RED, (START_POS_x, START_POS_y), 5, 5)                  # Draw start position
    pg.draw.circle(WIN, BLUE, (END_POS_x, END_POS_y), 5, 5)                     # Draw end position

    WIN.blit(pg.transform.rotate(DRONE, drone_angle), (drone.x, drone.y))

def hit_wall(wall_top, wall_bottom, drone):
    for wall in [*wall_top, *wall_bottom]:
        if drone.colliderect(wall):
            pg.event.post(pg.event.Event(HIT_WALL))                             # When the drone collide with the wall it will create a event called HIT_WALL

def main():
    WIN.fill(WHITE)
    drone = pg.Rect(START_POS_d_x, START_POS_d_y, DRONE_WIDTH, DRONE_HEIGHT)    # Set the drone as rectangle and give it a position, width and height
    drone_angle = 0 # Angle

    wall_top, wall_bottom = [], []
    width_game = WIDTH - START_POS_x - drone.width//2                           # Set a width where the walls should be spawn
    first_rect_pos = START_POS_x + drone.width//2 + 100                         # Set the x-position of the first wall

    for i in range(N_WALL):                                                     # Set the positions, width and heights of the walls
        pos_x = first_rect_pos + i * width_game//N_WALL
        pos_y_top = 0
        width = 50
        height_top = random.randint(0, HEIGHT - 100)
        pos_y_bottom = height_top + 100
        height_bottom = HEIGHT - pos_y_bottom
        rectangle_top = (pos_x, pos_y_top, width, height_top)
        rectangle_bottom = (pos_x, pos_y_bottom, width, height_bottom)
        wall_top.append(rectangle_top)
        wall_bottom.append(rectangle_bottom)

    clock, run = pg.time.Clock(), True

    # Initialize planning and models
    SCALE = 100 # px to m

    draw_window(drone, drone_angle, wall_top, wall_bottom)

    obstacles = [*wall_top, *wall_bottom]
    planner = RRT([START_POS_x, START_POS_y], [END_POS_x, END_POS_y], [WIDTH, HEIGHT], obstacles=obstacles)

    pg.event.get()
    draw_f = lambda: draw_window(drone, 0, wall_top, wall_bottom)
    planner.pg, planner.WIN, planner.draw_f = pg, WIN, draw_f

    planner.plan()
    path = planner.usable_path(SCALE)

    trajectory = TrajectoryGeneration(path, obstacles=obstacles)
    trajectory.solve(SCALE)

    t, dt = 0, 0.01 # Simulation time

    model = Quadrotor()
    model.t_step = dt
    s = model.reset(position=trajectory.eval(0)[0])

    f_control = GeometricControl().control

    while run:                                                                  # Wait until the start button is pressed
        clock.tick(FPS)                                                         # Set a frame rate

        for event in pg.event.get():                                            # Interrupt function
            if event.type == pg.QUIT or (event.type == pg.KEYUP and event.key == pg.K_ESCAPE):
                run = False

        draw_window(drone, drone_angle, wall_top, wall_bottom)

        # Fly drone
        pos, vel, acc, yaw, yaw_dot = trajectory.eval(t)
        traj = pack_traj(pos, vel, acc=acc, yaw=yaw, yaw_dot=yaw_dot)
        s = model.step(f_control(traj, s))

        euler = Rotation.from_quat(s['q']).as_euler('xyz')*180/np.pi
        drone.x, drone.y, drone_angle = s['x'][0]*SCALE, s['x'][2]*SCALE, euler[1]

        drone.x -= DRONE_WIDTH/2
        drone.y -= DRONE_HEIGHT/2

        # Draw path
        planner.visualize()
        WIN.blit(pg.transform.rotate(DRONE, drone_angle), (drone.x, drone.y))

        # Draw trajectory
        dt = 0.01
        p, _, _, _, _ = trajectory.eval(0)
        for tt in np.arange(dt, trajectory.t_total, dt):
            pn, _, _, _, _ = trajectory.eval(tt)
            pg.draw.line(WIN, RED, (p[0]*SCALE, p[2]*SCALE), (pn[0]*SCALE, pn[2]*SCALE), 3)

            p = pn

        pg.display.update()
        t += dt

    pg.quit()

if __name__ == "__main__":
    main()
