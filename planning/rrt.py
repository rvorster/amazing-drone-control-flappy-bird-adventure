import numpy as np
import rtree
import time

# Simple node helpers
class Node():
    def __init__(self, arr):
        self.cost = 1e10 # Initialize with really high value
        self.parent = None

        self.arr = np.array(arr)
        self.dim = self.arr.shape[0]

        # Append zeros
        self.x, self.y, self.z = np.hstack([self.arr, np.zeros(3 - self.dim)])

class NodeTree():
    def __init__(self, n=2):
        self.n = n
        self.nodes = []

        # Init tree
        p = rtree.index.Property()
        p.dimension = n
        self.tree = rtree.index.Index(properties=p)

    @property
    def len(self):
        return len(self.nodes)

    def add(self, new_node):
        self.tree.insert(self.len, (*new_node.arr, ))
        self.nodes.append(new_node)

    def nearest_k(self, node, k):
        idxs = self.tree.nearest((*node.arr, ), k)
        return [self.nodes[idx] for idx in idxs]

    def nearest(self, node):
        return self.nearest_k(node, 1)[0]

# Compute distance between node and some other node
def dist(n1, n2):
    return np.linalg.norm(n1.arr - n2.arr)

# Compute total cost of path
def cost(n1, n2):
    return n1.cost + dist(n1, n2)

def ccw(A, B, C): # See for more information https://bryceboe.com/2006/10/23/line-segment-intersection-algorithm/
    return (C.y - A.y) * (B.x - A.x) > (B.y - A.y) * (C.x - A.x)

class RRT():
    def __init__(self, start, goal, bounds, informed=True, dim=2, obstacles=[], margins=[],
                 step=500, radius=1000, n_nodes=2000, pg=None, WIN=None, draw_f=None):
        if not (dim == 2 or dim == 3):
            raise Exception('Only 2D and 3D are supported!')

        self.dim = dim
        self.start, self.goal = Node(start), Node(goal)

        self.obstacles = obstacles

        # Bounds (assume rect viewport starting at origin
        self.coords_min, self.coords_max = np.zeros(self.dim), bounds
        assert self.within_bound(self.start) and self.within_bound(self.goal)

        self.c_min = dist(self.start, self.goal) # Minimum cost
        self.x_center = (self.start.arr + self.goal.arr)/2

        self.informed = informed
        self.ellipse_rot, self.L = self.gen_ellipse(), []

        # Settings
        self.n_nodes = n_nodes # Maximum number of nodes

        self.step = step # Maximum step size for new node
        self.radius = radius # Search radius

        self.margins = np.array(margins) if margins != [] else np.zeros(self.dim)

        # State
        self.nodes, self.path = [], []

        # Pygame drawing
        self.pg, self.WIN, self.draw_f = pg, WIN, draw_f

    def plan(self):
        self.start.cost = 0

        self.nodes = NodeTree(n=self.dim)
        self.nodes.add(self.start)

        c_best = np.inf # Current best path length
        final_nodes = [] # Choose from this list of final nodes

        for i in range(self.n_nodes):
            perc = 100*i/self.n_nodes
            print(f'RRT: {perc:.2f}%', end='\r')

            # Sample new node
            rand_node = self.sample(c_best)
            if not rand_node:
                break

            # Find nearest node
            nearest_node = self.nodes.nearest(rand_node)
            new_node = self.step_between(nearest_node, rand_node)

            if not self.check_not_intersect(nearest_node, new_node):
                continue

            # Choose parent
            n_near_nodes = int(25*np.log(self.nodes.len + 1)) # Optimality criterion
            all_near_nodes = self.nodes.nearest_k(new_node, n_near_nodes)

            self.choose_parent(new_node, all_near_nodes)

            self.nodes.add(new_node)

            # Rewire
            for near_node in all_near_nodes:
                self.choose_parent(near_node, [new_node])

            # And propagate
            self.recurse(new_node)

            # Check if close to goal
            if dist(new_node, self.goal) <= self.radius and self.check_not_intersect(new_node, self.goal):
                final_nodes.append(new_node)
                self.choose_parent(self.goal, final_nodes)

                c_best = self.goal.cost

            # Draw
            if self.pg is not None:
                self.draw_f()
                for n in [self.goal] + self.nodes.nodes:
                    if n.parent is not None:
                        self.pg.draw.line(self.WIN, (0,0,0), n.parent.arr, n.arr)

                if self.goal.parent is not None:
                    # Final path
                    n = self.goal
                    while n != self.start:
                        self.pg.draw.line(self.WIN, (255, 255, 255), n.parent.arr, n.arr, 3)
                        n = n.parent

                    # Ellipse
                    if self.L != [] and self.informed:
                        xs = np.arange(-1, 1.0, 0.001)
                        ys = np.sqrt(1 - xs**2)

                        xs = np.vstack([xs, ys]).T
                        for (x, xx) in zip(xs[1:], xs[:-1]):
                            xt = self.x_center + self.ellipse_rot@(self.L@x.T)
                            xxt = self.x_center + self.ellipse_rot@(self.L@xx.T)

                            self.pg.draw.line(self.WIN, (0, 0, 255), xt, xxt, 3)

                            x, xx = np.copy(x), np.copy(xx)
                            x[1] *= -1
                            xx[1] *= -1
                            xt = self.x_center + self.ellipse_rot@(self.L@x.T)
                            xxt = self.x_center + self.ellipse_rot@(self.L@xx.T)

                            self.pg.draw.line(self.WIN, (0, 0, 255), xt, xxt, 3)

                self.pg.display.update()

        if self.goal.parent is None:
            raise Exception('No path found!')

        print(f"Got solution with cost: {c_best:.3f}")

        # Get final solution by backtracking
        n = self.goal
        final_path = [n]
        while n != self.start:
            n = n.parent
            final_path.append(n)

        self.path = final_path
        return final_path

    def usable_path(self, scale):
        return np.array(list(reversed([n.arr for n in self.path])))/scale

    def recurse(self, parent_node):
        for n in self.nodes.nodes:
            if n.parent == parent_node:
                n.cost = cost(parent_node, n)
                self.recurse(n)

    # Choose nearest node in terms of cost
    def choose_parent(self, new_node, candidate_nodes):
        for n in candidate_nodes:
            c = cost(n, new_node)
            if c < new_node.cost and self.check_not_intersect(n, new_node):
                new_node.cost, new_node.parent = c, n

    def gen_ellipse(self):
        # Directly from informed rrt paper
        e = (self.goal.arr - self.start.arr)/self.c_min
        w = np.zeros(self.dim)
        w[0] = 1

        m = np.outer(e, w)
        s, _, d = np.linalg.svd(m)

        m = np.eye(self.dim)
        m[-1,-1] = np.linalg.det(s)*np.linalg.det(d)

        return s@m@d.T

    def sample(self, c_best):
        if self.goal.parent is None or not self.informed:
            x = np.random.uniform(low=self.coords_min, high=self.coords_max, size=(self.dim,))
        else:
            if abs(c_best - self.c_min) < 1e-5:
                return False

            # http://compneuro.uwaterloo.ca/files/publications/voelker.2017.pdf
            r = np.sqrt(c_best**2 - self.c_min**2)/2
            self.L = np.diag(np.hstack([[c_best/2], np.ones(self.dim - 1)*r]))

            x = np.ones(self.dim)*np.inf
            while True:
                x = np.random.normal(0, 1, (1, self.dim + 2))
                x = (x/np.linalg.norm(x))[0, :self.dim]

                x = self.x_center + (self.ellipse_rot@self.L@x.T).T
 
                if self.within_bound(Node(x)):
                    break

        return Node(x)

    # Step towards nearest node or create new node one step away
    def step_between(self, n1, n2):
        if dist(n1, n2) <= self.step:
            return n2

        # Cut short at some euclidian distance
        return Node(n1.arr + n2.arr*self.step/np.linalg.norm(n2.arr))

    def within_bound(self, n):
        return np.all(n.arr >= self.coords_min) and np.all(n.arr <= self.coords_max)

    # Check if the line between nodes intersect an obstacle
    def check_not_intersect(self, node1, node2):
        if not self.within_bound(node1) or not self.within_bound(node2):
            return False

        # Really beun intersection method
        if self.dim == 3:
            n_points = 50
            def check(obs, path):
                obs += np.hstack([-self.margins, 2*self.margins])

                x, y, z, xx, yy, zz = obs
                xt, yt, zt = x + xx, y + yy, z + zz
                for p in path:
                    if p[0] <= xt and p[0] >= x and p[1] <= yt and p[1] >= y and p[2] <= zt and p[2] >= z:
                        return False

                return True

            path = np.linspace(node1.arr, node2.arr, n_points)
            for obs in self.obstacles:
                if not check(obs, path):
                    return False

            return True

        margin_width, margin_height = 10, 10

        # The obstacles will be a list with the position, width and height of the obstacles, so (pos.x, pos.y, width, height)
        for i, cor in enumerate(self.obstacles):
            top_left = Node([cor[0] - margin_width, cor[1] - margin_height])
            top_right = Node([cor[0] + cor[2], cor[1] - margin_height])
            bottom_left = Node([cor[0] - margin_width, cor[1] + cor[3]])
            bottom_right = Node([cor[0] + cor[2], cor[1] + cor[3]])

            intersect1 = ccw(node1, top_left, bottom_left) != ccw(node2, top_left, bottom_left) and ccw(node1, node2, top_left) != ccw(node1, node2, bottom_left)
            if intersect1: return False

            intersect2 = ccw(node1, top_left, top_right) != ccw(node2, top_left, top_right) and ccw(node1, node2, top_left) != ccw(node1, node2, top_right)
            if intersect2: return False

            intersect3 = ccw(node1, bottom_right, top_right) != ccw(node2, bottom_right, top_right) and ccw(node1, node2, bottom_right) != ccw(node1, node2, top_right)
            if intersect3: return False

            intersect4 = ccw(node1, bottom_right, bottom_left) != ccw(node2, bottom_right, bottom_left) and ccw(node1, node2, bottom_right) != ccw(node1, node2, bottom_left)
            if intersect4: return False

        return True

    def visualize(self):
        for n in self.nodes.nodes:
            if n.parent is not None:
                self.pg.draw.aaline(self.WIN, (0, 0, 0), n.parent.arr, n.arr, 1)

        for n in self.path:
            if n.parent is not None:
                self.pg.draw.line(self.WIN, (255, 255, 255), n.parent.arr, n.arr, 3)
