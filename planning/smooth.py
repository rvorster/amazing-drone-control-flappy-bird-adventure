import numpy as np
from scipy.optimize import minimize

from itertools import product

def xz_to_3d(x): return np.array([x[0], 0, x[1]])

class TrajectoryGeneration():
    def __init__(self, path, obstacles=[], margins=[], dt=0.01, max_speed=2,
                 max_iter=500, order_opt=4, gamma=2e2, constrain=False):
        # Settings
        self.max_speed = max_speed
        self.order_opt, self.order = order_opt, 2*(order_opt + 1)

        self.gamma = gamma
        self.dt = dt

        self.constrain = constrain
        self.max_iter = max_iter

        self.obstacles = obstacles
        self.margins = margins

        # Path
        self.set_path(path)

    def set_path(self, path):
        self.path = path
        [self.path_len, self.dim] = self.path.shape
        if not (self.dim == 2 or self.dim == 3):
            raise Exception('Only 2D and 3D are supported!')

        self.n = self.path_len - 1 # Number of segments
        self.s = self.order*self.n # Matrix size

        self.margins = np.array(self.margins) if self.margins != [] else np.zeros(self.dim)

        self.yaw, self.h = 0, [0, 0]

    def solve(self, scale):
        def check():
            for t in np.arange(0, self.t_total, self.dt):
                idx = self.get_segment_idx(t)
                pos, _, _, _, _ = self.eval(t)

                pos *= scale
                for obs in self.obstacles:
                    if self.dim == 2:
                        if pos[0] >= obs[0] and pos[0] <= (obs[0] + obs[2]) \
                                and pos[2] >= obs[1] and pos[2] <= (obs[1] + obs[3]):
                            return idx
                    else:
                        # Dim = 3
                        x, y, z, xx, yy, zz = obs
                        xt, yt, zt = x + xx, y + yy, z + zz

                        p = pos
                        if p[0] <= xt and p[0] >= x and p[1] <= yt and p[1] >= y and p[2] <= zt and p[2] >= z:
                            return idx

            return -1

        while True:
            self.optimize()
            idx = check()
            if idx == -1:
                break

            print('Recalculating trajectory due to collision!')

            new_node = (self.path[idx] + self.path[idx + 1])/2
            self.set_path(np.insert(self.path, idx + 1, new_node, axis=0))

        print(f'Got solution with total time for drone to travel: {self.t_total:.2f}')

    def get_segment_idx(self, t):
        return np.where(t >= self.ts)[0][-1]

    def eval(self, t):
        if t >= self.t_total:
            t = self.t_total - 0.0001

        idx = self.get_segment_idx(t)
        t -= self.ts[idx]

        coeffs = self.coeffs[:, self.order*idx:self.order*(idx + 1)]
        pos = coeffs@self.gen_poly_derivatives(t, k=0)
        vel = coeffs@self.gen_poly_derivatives(t, k=1)
        acc = coeffs@self.gen_poly_derivatives(t, k=2)

        # Calculate yaw angle
        self.yaw, yaw_dot = 0, 0
        if self.dim == 3:
            v = vel[:2] # Only interested in xy plane
            h = v/np.linalg.norm(v)

            d_yaw = np.arccos(np.dot(self.h, h)) # Cos rule
            self.yaw += d_yaw*np.sign(np.cross(self.h, h))
            self.h = h # Set new heading

            # Round to nearest 2pi
            self.yaw = self.yaw - np.sign(self.yaw)*(np.abs(self.yaw) > np.pi)*2*np.pi
            yaw_dot = d_yaw

        # Convert coordinates for model/controller if running in 2D
        if self.dim == 2:
            pos, vel, acc = xz_to_3d(pos), xz_to_3d(vel), xz_to_3d(acc)

        return pos, vel, acc, self.yaw, yaw_dot

    def optimize(self):
        # Minimum time per segment
        tm = np.linalg.norm(self.path[:-1] - self.path[1:], ord=2, axis=1)/self.max_speed

        constraints = []
        if self.constrain:
            constraints.append({'type': 'ineq', 'fun': lambda t: t - tm}) # tm as lower bound

        solution = minimize(self.cost, tm, options={'maxiter': self.max_iter}, constraints=constraints)

        # Final solution
        self.t = solution['x']

        self.ts = np.hstack([0, np.cumsum(self.t)])
        self.t_total = self.ts[-1]

        coeffs, self.total_cost = self.generate(self.t)
        self.coeffs = coeffs.T

    def cost(self, t):
        _, cost = self.generate(t)
        return cost + self.gamma*t.sum()

    def generate(self, t):
        Q = self.Q(t)
        A, A_inv = self.A(t)
        b = self.b()

        # Compute R matrix
        R = A_inv.T@Q@A_inv

        n_u = self.order_opt*(self.path_len - 2) # Number of unknowns

        # Segment R
        R_ff, R_pp = R[:-n_u, :-n_u], R[-n_u:, -n_u:]
        R_fp, R_pf = R[:-n_u, -n_u:], R[-n_u:, :-n_u]

        # Sort b
        b[-n_u:, :] = -np.linalg.inv(R_pp)@R_fp.T@b[:-n_u, :]

        P = A_inv@b # Solve
        return P, np.trace(P.T@Q@P)

    # Compose Hessian matrix (Q) -> square of polynomial w.r.t its coefficients scaled to time
    def Q(self, t):
        Q = np.zeros((self.s, self.s))

        # Pre-calc some stuff
        r = np.array(list(range(self.order_opt)))
        ij = list(filter(lambda xs: all(x >= self.order_opt for x in xs), product(range(self.order), repeat=2)))

        terms = [2*np.prod((i - r)*(j - r)) for (i, j) in ij]
        powers = [1 + i + j - 2*self.order_opt for (i, j) in ij]

        for k in range(self.n):
            for ii, (i, j) in enumerate(ij):
                power, term = powers[ii], terms[ii]
                Q[self.order*k + i, self.order*k + j] = term*(t[k]**power)/power

        return Q

    # Compute b vector
    def b(self):
        b = np.zeros((self.s, self.dim))
        b[:self.n, :] = self.path[:-1, :]
        b[self.n:2*self.n, :] = self.path[1:, :]

        return b

    def gen_poly_derivatives(self, x, k=0):
        coef = np.polyder(np.ones(self.order), k)[::-1]

        poly = np.zeros(self.order)
        poly[k:] = coef*np.power(x, np.arange(0, self.order - k))

        return poly

    # All the polynomial constraints
    def A(self, t):
        A = np.zeros((self.s, self.s))

        # Set zero order constraints
        for i in range(self.n):
            A[i, self.order*i:self.order*(i + 1)] = self.gen_poly_derivatives(0, k=0)
            A[i + self.n, self.order*i:self.order*(i + 1)] = self.gen_poly_derivatives(t[i], k=0)

        # Set derivative constraints
        start = 2*self.n
        for i in range(self.n - 1):
            rows = [start + self.order_opt*i, start + self.order_opt*(i + 1)]
            for ir, row in enumerate(np.arange(*rows)):
                poly_order = ir + 1 # Index + 1 = order of this constraint

                A[row, self.order*i:self.order*(i + 1)] = -self.gen_poly_derivatives(t[i], k=poly_order)
                A[row, self.order*(i + 1):self.order*(i + 2)] = self.gen_poly_derivatives(0, k=poly_order)

        # All zeros at the beginning + end
        rows = [rows[1], rows[1] + self.order_opt]
        for ir, row in enumerate(np.arange(*rows)):
            poly_order = ir + 1

            A[row, :self.order] = self.gen_poly_derivatives(0, k=poly_order) # Start of path
            A[row + self.order_opt, -self.order:] = self.gen_poly_derivatives(t[-1], k=poly_order) # End of path

        start = rows[1]
        for i in range(1, self.n):
            rows = [start + self.order_opt*i, start + self.order_opt*(i + 1)]
            for ir, row in enumerate(np.arange(*rows)):
                poly_order = ir + 1
                A[row, self.order*i:self.order*(i + 1)] = self.gen_poly_derivatives(0, k=poly_order)

        return A, np.linalg.inv(A)

# Testing
if __name__ == '__main__':
    test_path = np.array([[5, 5], [5, 6], [6, 6]])
    traj = TrajectoryGeneration(test_path)

    traj.optimize()