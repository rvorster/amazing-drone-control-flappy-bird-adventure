import numpy as np
import matplotlib.pyplot as plt

from control.controller import GeometricControl
from control.model import Quadrotor
from trajectories.dummy import f_trajectory_dummy
import sim_plotting

model = Quadrotor()

def sim(f_trajectory, f_control, dt=0.01, render=True):
    # Intialize
    model.t_step = dt
    s = model.reset(position=f_trajectory(0, dt)['x'])

    # Log
    pos, pos_des = [], []
    ts = []

    t = 0
    while (t < 5):
        traj = f_trajectory(t, dt)
        s = model.step(f_control(traj, s))

        # Log
        pos.append(s['x'])
        pos_des.append(traj['x'])

        # Update
        ts.append(t)
        t += dt

    ts = np.array(ts)
    pos, pos_des = np.array(pos), np.array(pos_des)

    if render:
        sim_plotting.render(pos, pos_des)

if __name__ == '__main__':
    f_control = GeometricControl().control
    f_trajectory = f_trajectory_dummy

    sim(f_trajectory, f_control, render=True)