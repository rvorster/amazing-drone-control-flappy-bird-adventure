

import time
import numpy as np
import os
import random

from planning.rrt import RRT, Node
from trajectories.utils import pack_traj

# np.random.seed(42)
random.seed(250)

WIDTH, HEIGHT = 1500, 740

FPS = 60
VEL = 5
N_WALL = 5
ANGLE = 0

DRONE_WIDTH, DRONE_HEIGHT = 90, 18

START_POS_x, START_POS_y = 100, 100                                             # Start position
END_POS_x, END_POS_y = 650, 700                                                  # End position

width_game = WIDTH - START_POS_x - DRONE_WIDTH//2
first_rect_pos = START_POS_x + DRONE_WIDTH//2 + 100

wall_top = []
wall_bottom = []
for i in range(N_WALL):                                                     # Set the positions, width and heights of the walls
    pos_x = first_rect_pos + i * width_game//N_WALL
    pos_y_top = 0
    width = 50
    height_top = random.randint(0, HEIGHT - 100)
    pos_y_bottom = height_top + 100
    height_bottom = HEIGHT - pos_y_bottom
    rectangle_top = (pos_x, pos_y_top, width, height_top)
    rectangle_bottom = (pos_x, pos_y_bottom, width, height_bottom)
    wall_top.append(rectangle_top)
    wall_bottom.append(rectangle_bottom)

obstacles = [*wall_top, *wall_bottom]

#Rapid fire testing
rrt_test = np.array(["n_nodes","cost","time", "informed"])

#Nodes to test goes here
n_nodes = [500, 750, 1000, 1250, 1500, 2500, 5000]
informed = [True, False]
n_iter = 2

for nodes in n_nodes:
    #Number of times to iterate per number of nodes
    for j in range(n_iter):
        # np.random.seed(j)
        for v in informed:
            planner = RRT([START_POS_x, START_POS_y], [END_POS_x, END_POS_y], [WIDTH, HEIGHT], obstacles=obstacles, n_nodes=nodes, informed=v)
            try:
                start = time.time()
                path = planner.plan()
                time_elapsed = time.time() - start

                # Slap all that stuff into an array
                append = np.array([nodes,planner.goal.cost,time_elapsed, v])
                rrt_test = np.vstack((rrt_test,append))
                print(nodes, v, time_elapsed)
            except:
                print('No path found!')
                planner.goal.cost, time_elapsed = 0, 0


# Save the array here
print(rrt_test)
np.savetxt("rrt_results",rrt_test,fmt="%s")
